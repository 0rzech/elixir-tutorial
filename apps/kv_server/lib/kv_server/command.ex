defmodule KVServer.Command do
  @doc ~S"""
  Parses the given `line` into a command.

  ## Examples

    iex> KVServer.Command.parse("CREATE shopping\n")
    {:ok, {:create, "shopping"}}

    iex> KVServer.Command.parse("CREATE  shopping  \n")
    {:ok, {:create, "shopping"}}

    iex> KVServer.Command.parse("DELETE shopping\n")
    {:ok, {:delete, "shopping"}}

    iex> KVServer.Command.parse("PUT shopping milk 1\n")
    {:ok, {:put, "shopping", "milk", "1"}}

    iex> KVServer.Command.parse("GET shopping milk\n")
    {:ok, {:get, "shopping", "milk"}}

    iex> KVServer.Command.parse("DELETE shopping eggs\n")
    {:ok, {:delete, "shopping", "eggs"}}

  Unknown commands or commands with the wrong number of arguments return an error:

    iex> KVServer.Command.parse("UNKNOWN shopping eggs\n")
    {:error, :unknown_command}

    iex> KVServer.Command.parse("GET shopping\n")
    {:error, :unknown_command}

  """
  def parse(line) do
    case String.split(line) do
      ["CREATE", bucket] -> {:ok, {:create, bucket}}
      ["DELETE", bucket] -> {:ok, {:delete, bucket}}
      ["PUT", bucket, key, value] -> {:ok, {:put, bucket, key, value}}
      ["GET", bucket, key] -> {:ok, {:get, bucket, key}}
      ["DELETE", bucket, key] -> {:ok, {:delete, bucket, key}}
      _ -> {:error, :unknown_command}
    end
  end

  @doc """
  Runs the given command.
  """
  def run(command, registry \\ KV.Registry)

  def run({:create, bucket}, registry) do
    case KV.Router.route(bucket, KV.Registry, :create, [registry, bucket]) do
      {:ok, _} -> {:ok, "OK\n"}
      :error -> {:error, :already_exists}
    end
  end

  def run({:delete, bucket}, registry) do
    case KV.Router.route(bucket, KV.Registry, :delete, [registry, bucket]) do
      :ok -> {:ok, "OK\n"}
      :error -> {:error, :not_found}
    end
  end

  def run({:get, bucket, key}, registry) do
    lookup(bucket, registry, fn pid ->
      value = KV.Bucket.get(pid, key)
      {:ok, "#{value}\nOK\n"}
    end)
  end

  def run({:put, bucket, key, value}, registry) do
    lookup(bucket, registry, fn pid ->
      KV.Bucket.put(pid, key, value)
      {:ok, "OK\n"}
    end)
  end

  def run({:delete, bucket, key}, registry) do
    lookup(bucket, registry, fn pid ->
      KV.Bucket.delete(pid, key)
      {:ok, "OK\n"}
    end)
  end

  defp lookup(bucket, registry, callback) do
    case KV.Router.route(bucket, KV.Registry, :lookup, [registry, bucket]) do
      {:ok, pid} -> callback.(pid)
      :error -> {:error, :not_found}
    end
  end
end

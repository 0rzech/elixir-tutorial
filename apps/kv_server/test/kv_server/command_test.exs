defmodule KVServer.CommandTest do
  use ExUnit.Case, async: true
  doctest KVServer.Command

  setup do
    :ok = Application.put_env(:kv, :routing_table, [{?a..?z, node()}])
  end

  setup context do
    _ = start_supervised!({KV.Registry, name: context.test})
    %{registry: context.test}
  end

  test "parses commands" do
    assert KVServer.Command.parse("CREATE zakupy") == {:ok, {:create, "zakupy"}}
    assert KVServer.Command.parse("DELETE zakupy") == {:ok, {:delete, "zakupy"}}
    assert KVServer.Command.parse("PUT zakupy mleko 1") == {:ok, {:put, "zakupy", "mleko", "1"}}
    assert KVServer.Command.parse("GET zakupy mleko") == {:ok, {:get, "zakupy", "mleko"}}
    assert KVServer.Command.parse("DELETE zakupy mleko") == {:ok, {:delete, "zakupy", "mleko"}}
    assert KVServer.Command.parse("UNKNOWN command") == {:error, :unknown_command}
    assert KVServer.Command.parse("CREATE zakupy mleko") == {:error, :unknown_command}
  end

  test "creates and deletes buckets", %{registry: registry} do
    assert {:ok, bucket} = KVServer.Command.run({:create, "zakupy"}, registry)
    assert KVServer.Command.run({:create, "zakupy"}, registry) == {:error, :already_exists}

    assert {:ok, bucket} = KVServer.Command.run({:create, "zadania"}, registry)
    assert KVServer.Command.run({:create, "zadania"}, registry) == {:error, :already_exists}

    assert {:ok, msg} = KVServer.Command.run({:delete, "zadania"}, registry)
    assert KVServer.Command.run({:delete, "zadania"}, registry) == {:error, :not_found}

    assert {:ok, msg} = KVServer.Command.run({:delete, "zakupy"}, registry)
    assert KVServer.Command.run({:delete, "zakupy"}, registry) == {:error, :not_found}
  end

  test "manages bucket entries", %{registry: registry} do
    {:ok, _} = KVServer.Command.run({:create, "zakupy"}, registry)

    assert {:ok, msg} = KVServer.Command.run({:put, "zakupy", "mleko", 2}, registry)
    assert KVServer.Command.run({:get, "zakupy", "mleko"}, registry) == {:ok, "2\nOK\n"}

    assert {:ok, msg} = KVServer.Command.run({:delete, "zakupy", "mleko"}, registry)
    assert KVServer.Command.run({:get, "zakupy", "mleko"}, registry) == {:ok, "\nOK\n"}
  end
end

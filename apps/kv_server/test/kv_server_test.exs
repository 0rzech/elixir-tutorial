defmodule KVServerTest do
  use ExUnit.Case
  @moduletag :capture_log
  doctest KVServer

  setup do
    Application.stop(:kv)
    :ok = Application.start(:kv)
  end

  setup do
    opts = [:binary, packet: :line, active: false]
    {:ok, socket} = :gen_tcp.connect('localhost', 4040, opts)
    %{socket: socket}
  end

  test "greets the world" do
    assert KVServer.hello() == :world
  end

  test "server interaction", %{socket: socket} do
    assert send_and_recv(socket, "UNKNOWN zakupy\n") == "UNKNOWN COMMAND\n"
    assert send_and_recv(socket, "GET zakupy jajka\n") == "BUCKET NOT FOUND\n"
    assert send_and_recv(socket, "DELETE zakupy\n") == "BUCKET NOT FOUND\n"
    assert send_and_recv(socket, "CREATE zakupy\n") == "OK\n"
    assert send_and_recv(socket, "PUT zakupy jajka 3\n") == "OK\n"
    # GET returns two lines
    assert send_and_recv(socket, "GET zakupy jajka\n") == "3\n"
    assert send_and_recv(socket, "") == "OK\n"
    assert send_and_recv(socket, "DELETE zakupy jajka\n") == "OK\n"
    assert send_and_recv(socket, "DELETE zakupy\n") == "OK\n"
  end

  def send_and_recv(socket, command) do
    :ok = :gen_tcp.send(socket, command)
    {:ok, data} = :gen_tcp.recv(socket, 0, 1000)
    data
  end
end

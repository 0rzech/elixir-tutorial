defmodule KV.BucketTest do
  use ExUnit.Case, async: true

  setup do
    {:ok, bucket} = KV.Bucket.start_link([])
    %{bucket: bucket}
  end

  test "stores values by key", %{bucket: bucket} do
    assert KV.Bucket.get(bucket, "mleko") == nil

    KV.Bucket.put(bucket, "mleko", 3)
    assert KV.Bucket.get(bucket, "mleko") == 3

    assert KV.Bucket.delete(bucket, "mleko") == 3
    assert KV.Bucket.delete(bucket, "mleko") == nil
    assert KV.Bucket.get(bucket, "mleko") == nil
  end

  test "are temporary workers" do
    assert Supervisor.child_spec(KV.Bucket, []).restart == :temporary
  end
end

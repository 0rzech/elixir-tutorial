defmodule KV.RegistryTest do
  use ExUnit.Case, async: true

  setup context do
    _ = start_supervised!({KV.Registry, name: context.test})
    %{registry: context.test}
  end

  test "spawns buckets", %{registry: registry} do
    assert KV.Registry.lookup(registry, "zakupy") == :error

    assert {:ok, bucket} = KV.Registry.create(registry, "zakupy")
    assert {:ok, bucket} = KV.Registry.lookup(registry, "zakupy")
    assert KV.Registry.create(registry, "zakupy") == :error

    KV.Bucket.put(bucket, "mleko", 1)
    assert KV.Bucket.get(bucket, "mleko") == 1
  end

  test "removes buckets", %{registry: registry} do
    KV.Registry.create(registry, "zakupy")
    assert {:ok, bucket} = KV.Registry.lookup(registry, "zakupy")

    assert KV.Registry.delete(registry, "zakupy") == :ok
    # Do a call to ensure the registry processed the DOWN message
    _ = KV.Registry.create(registry, "udawany")
    assert KV.Registry.lookup(registry, "zakupy") == :error
    assert KV.Registry.delete(registry, "zakupy") == :error
  end

  test "removes buckets on exit", %{registry: registry} do
    KV.Registry.create(registry, "zakupy")
    {:ok, bucket} = KV.Registry.lookup(registry, "zakupy")
    Agent.stop(bucket)

    # Do a call to ensure the registry processed the DOWN message
    _ = KV.Registry.create(registry, "udawany")
    assert KV.Registry.lookup(registry, "zakupy") == :error
  end

  test "removes bucket on crash", %{registry: registry} do
    KV.Registry.create(registry, "zakupy")
    {:ok, bucket} = KV.Registry.lookup(registry, "zakupy")

    # Stop the bucket with non-normal reason
    Agent.stop(bucket, :shutdown)

    # Do a call to ensure the registry processed the DOWN message
    _ = KV.Registry.create(registry, "udawany")
    assert KV.Registry.lookup(registry, "zakupy") == :error
  end

  test "bucket can crash at any time", %{registry: registry} do
    KV.Registry.create(registry, "zakupy")
    {:ok, bucket} = KV.Registry.lookup(registry, "zakupy")

    # Simulate a bucket crash by explicitly and synchronously shutting it down
    Agent.stop(bucket, :shutdown)

    # Now trying to call the dead process causes a :noproc exit
    catch_exit(KV.Bucket.put(bucket, "mleko", 3))
  end
end

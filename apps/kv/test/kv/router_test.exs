defmodule KV.RouterTest do
  use ExUnit.Case, async: true

  @tag :distributed
  test "routes requests across nodes" do
    # Replace computer-name with your local machine nodes.
    assert KV.Router.route("cześć", Kernel, :node, []) == :foo@computer-name
    assert KV.Router.route("wszechświecie", Kernel, :node, []) == :bar@computer-name
  end

  test "raises on unknown entries" do
    assert_raise RuntimeError, ~r/could not find entry/, fn ->
      KV.Router.route(<<0>>, Kernel, :node, [])
    end
  end
end

defmodule KV.Registry do
  use GenServer

  ## Client API

  @doc """
  Starts the registry with the given options.

  `:name` is always required.
  """
  def start_link(opts) do
    server = Keyword.fetch!(opts, :name)
    GenServer.start_link(__MODULE__, server, opts)
  end

  @doc """
  Creates a bucket associated with the given `name` in `server`.

  Returns `{:ok, pid}` if the bucket was created, `:error` if it already exists.
  """
  def create(server, name) do
    GenServer.call(server, {:create, name})
  end

  @doc """
  Looks up the bucket pid for `name` stored in `server`.

  Returns `{:ok, pid}` if the bucket exists, `:error` otherwise.
  """
  def lookup(server, name) do
    case :ets.lookup(server, name) do
      [{^name, bucket}] -> {:ok, bucket}
      [] -> :error
    end
  end

  @doc """
  Deletes a bucket associated with the given `name` from `server`.

  Returns `:ok` if the bucket exists, `:error` otherwise.
  """
  def delete(server, name) do
    GenServer.call(server, {:delete, name})
  end

  @doc """
  Stops the registry.
  """
  def stop(server) do
    GenServer.stop(server)
  end

  ## Server Callbacks

  def init(table) do
    names = :ets.new(table, [:named_table, read_concurrency: true])
    refs = %{}
    {:ok, {names, refs}}
  end

  def handle_call({:create, name}, _from, {names, refs} = state) do
    case lookup(names, name) do
      {:ok, _} ->
        {:reply, :error, state}

      :error ->
        {:ok, bucket} = DynamicSupervisor.start_child(KV.BucketSupervisor, KV.Bucket)
        ref = Process.monitor(bucket)
        refs = Map.put(refs, ref, name)
        :ets.insert(names, {name, bucket})
        {:reply, {:ok, bucket}, {names, refs}}
    end
  end

  def handle_call({:lookup, name}, _from, {names, _} = state) do
    {:reply, Map.fetch(names, name), state}
  end

  def handle_call({:delete, name}, _from, {names, _} = state) do
    case lookup(names, name) do
      {:ok, bucket} ->
        _ = DynamicSupervisor.terminate_child(KV.BucketSupervisor, bucket)
        {:reply, :ok, state}

      :error ->
        {:reply, :error, state}
    end
  end

  def handle_info({:DOWN, ref, :process, _pid, _reason}, {names, refs}) do
    {name, refs} = Map.pop(refs, ref)
    :ets.delete(names, name)
    {:noreply, {names, refs}}
  end

  def handle_info(_msg, state) do
    {:noreply, state}
  end
end
